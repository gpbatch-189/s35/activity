const express = require('express')

const dotenv = require('dotenv')

const mongoose = require('mongoose')

const app = express()

const port = 3001

dotenv.config()

mongoose.connect(`mongodb+srv://admin123:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.1tup4.mongodb.net/S35-Activity?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)	

let db = mongoose.connection

db.on('error', console.error.bind(console,"Connection Error"))
db.on('open', () => console.log('Connected to MongoDB!'))


// CREATING A SCHEMA

const userSchema = new mongoose.Schema({
	username : String,
	password : String
})

// CREATING A MODEL

const User = mongoose.model('User', userSchema)

//ROUTES
app.use(express.json()) 
app.use(express.urlencoded({extended: true}))

app.post('/signup', (request, response) => {
	User.findOne({username : request.body.username}, (error, result) => {
		if(result !== null && result.username == request.body.username){
			return response.send('Duplicate user found!')
		} else {
			if(request.body.username !== '' && request.body.password !== ''){
				let newUser = new User({
					username: request.body.username,
					password: request.body.password
				})
				newUser.save((error, savedUser) => {
					if(error){
						return response.send(error)
					}
					return response.send('New user has been registered!')
				})
			} else {
				return response.send('BOTH Username AND Password must be provided.')
			}
		}
	})
})



app.listen(port,() => console.log(`Server is running at port ${port}`))